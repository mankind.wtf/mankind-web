using System;
using System.Net;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

public class BadResponseException : Exception{

    private class ErrorResponse {
        public int Code {get;set;}
        public string Message {get;set;}= null;
    }

    public int StatusCode {get;}
    public override string Message {get;}

    public BadResponseException(HttpStatusCode statusCode, string content){
        this.StatusCode = (int) statusCode;

        bool err = false;
        var parsed = JsonConvert.DeserializeObject<ErrorResponse>(content, new JsonSerializerSettings{
            Error = delegate(object sender, ErrorEventArgs args){
                err = true;
            }
        });

        this.Message = err ? null : parsed.Message;
    }

}