using System.Threading.Tasks;
using System.Net.Http;
using System.Net.WebSockets;
using System.Net;
using System.IO;
using System.Threading;
using System.Text;
using Newtonsoft.Json;
using Microsoft.JSInterop;
using System;

public class Backend {

    private static readonly string PROD_REST_BASE_URL = "https://fsocietyback.singularity.net.za";
    private static readonly string PROD_WS_BASE_URL = "wss://fsocietyback.singularity.net.za";
    private static readonly string DEV_REST_BASE_URL = "http://localhost:4998";
    private static readonly string DEV_WS_BASE_URL = "ws://localhost:4998";


    private readonly HttpClient http;
    private readonly Observatory obz;
    private readonly JSIOp jsiop;

    private string playerAuth = null;
    private string playerNick = null;
    private string gameId = null;
    private string lastError = null;

    private ClientWebSocket client = null;

    public static bool IsDebug(){
        #if DEBUG
            return true;
        #else
            return false;
        #endif
    }

    private string getRestBaseUrl(){
        return IsDebug() ? DEV_REST_BASE_URL : PROD_REST_BASE_URL;
    }
    private string getWSBaseUrl(){
        return IsDebug() ? DEV_WS_BASE_URL : PROD_WS_BASE_URL;
    }

    public Backend(HttpClient http, Observatory obz, JSIOp jsiop) {
        this.http = http;
        this.obz = obz;
        this.jsiop = jsiop;
    }

    // Load content from cookie (if available)
    public async Task Initialise() {

        var pa = await jsiop.ReadCookie("playerAuth");
        var pn = await jsiop.ReadCookie("playerNick");
        var gi = await jsiop.ReadCookie("gameId");

        if (pa != "")
            this.playerAuth = pa;
        if (pn != "")
            this.playerNick = pn;
        if (gi != "")
            this.gameId = gi;

        if (IsDebug())
            Console.WriteLine("Game running in DEBUG mode. Debug output will be printed to console.");
        else {
            Console.WriteLine("================");
            Console.WriteLine("Hello fellow developer!");
            Console.WriteLine("If you would like to contribute to Mankind.wtf, check out the open source repos:");
            Console.WriteLine("https://bitbucket.org/stonepillarstudios/workspace/projects/FSOC");
            Console.WriteLine("or get in contact: contributing@mankind.wtf");
            Console.WriteLine("================");
        }
    }

    private async Task StorePlayer() {
        await jsiop.WriteCookie("playerAuth", this.playerAuth);
        await jsiop.WriteCookie("playerNick", this.playerNick);
        await jsiop.WriteCookie("gameId", this.gameId);
    }

    // Clears player auth and deletes cookie. Leaves player nick for future possible reuse.
    public async Task ClearPlayer() {
        this.playerAuth = null;
        this.gameId = null;

        await jsiop.ClearCookie("playerAuth");
        await jsiop.ClearCookie("gameId");
    }

    public async Task<long> CreateGame(string nick) {
        var resp = await http.PostAsync(getRestBaseUrl()+"/game", new StringContent($"{{\"nick\":\"{nick}\"}}"));

        string content = await resp.Content.ReadAsStringAsync();
        
        if (resp.StatusCode == HttpStatusCode.Created){
            var parsed = JsonConvert.DeserializeObject<GameCreatedResponse>(content); 
            
            this.playerAuth = parsed.player_auth;
            this.playerNick = nick;
            this.gameId = parsed.game_id.ToString();
            await this.StorePlayer();

            return parsed.game_id;
        } else {
            throw new BadResponseException(resp.StatusCode, content);
        }
    }

    public async Task RegisterToGame(string nick, string gameId) {

        // check if already registered
        if (PlayerIsRegistered() && this.gameId == gameId && this.playerNick == nick)
            return;
        
        var resp = await http.PostAsync(getRestBaseUrl()+"/game/"+gameId, new StringContent($"{{\"nick\":\"{nick}\"}}"));
    
        string content = await resp.Content.ReadAsStringAsync();
        
        if (resp.StatusCode == HttpStatusCode.Created){
            var parsed = JsonConvert.DeserializeObject<GameCreatedResponse>(content); 
            
            this.playerAuth = parsed.player_auth;
            this.playerNick = nick;
            this.gameId = gameId;
            await this.StorePlayer();

        } else {
            throw new BadResponseException(resp.StatusCode, content);
        }
    }

    public async Task SuggestCard(string text, bool quest){
        var qstr = quest ? "true" : "false"; // C# encodes to upper-case by default.
        await http.PostAsync(getRestBaseUrl()+"/suggestion", new StringContent($"{{\"text\":\"{text}\",\"question\":{qstr}}}"));
    }

    public bool PlayerIsRegistered() {
        return this.playerAuth != null;
    }

    public string GetPlayerNick(){
        return this.playerNick;
    }

    public string PopWSError(){
        var err = this.lastError;
        this.lastError = null;
        return err;
    }

    // Establish a connection to the game state
    // Only call this after either creating a game or registering the user to a game.
    // Will establish new connection if one already exists
    // Returns whether connection opened successfully.
    public async Task<bool> ConnectToGame(string gameId){
        
        client = new ClientWebSocket();

        try {
            
            await client.ConnectAsync(new Uri(getWSBaseUrl()+"/gamestate/"+gameId+"?auth="+playerAuth), CancellationToken.None);

            Task _ = receiverLoop(); // Runs async

        } catch (Exception e){
            if (IsDebug()) Console.WriteLine("Error connecting to game: {0}", e.Message);
            this.lastError = "Error connecting to game. Please create a new game or join a different game.";
            return false;
        }

        return true;
    }

    public void SendMessage(Message message){
        var textMsg = JsonConvert.SerializeObject(message);
        if (IsDebug())
            Console.WriteLine("WS Sending: {0}", textMsg);
        client.SendAsync(Encoding.UTF8.GetBytes(textMsg), WebSocketMessageType.Text, true, CancellationToken.None);
    }

    public async Task LeaveGame(){
        if (client != null && client.State == WebSocketState.Open){
            await client.CloseAsync(WebSocketCloseStatus.NormalClosure, "Player leavnig", CancellationToken.None);
            client.Dispose();
            if (IsDebug())
                Console.WriteLine("Connection closed");
        }
    }

    private async Task receiverLoop()
    {
        var buffer = new ArraySegment<byte>(new byte[2048]);
        do
        {
            WebSocketReceiveResult result;
            using (var ms = new MemoryStream())
            {
                do
                {
                    result = await client.ReceiveAsync(buffer, CancellationToken.None);
                    ms.Write(buffer.Array, buffer.Offset, result.Count);
                } while (!result.EndOfMessage);

                if (result.MessageType == WebSocketMessageType.Close)
                    break;

                ms.Seek(0, SeekOrigin.Begin);
                using (var reader = new StreamReader(ms, Encoding.UTF8))
                    messageParser(await reader.ReadToEndAsync());
            }
        } while (true);
    }

    private void messageParser(string msg){
        if (IsDebug())
            Console.WriteLine("WS Received: {0}", msg);
        
        try {
            var semiParsed = JsonConvert.DeserializeObject<Message>(msg);
            switch (semiParsed.type){
                case "STATUS":
                    obz.Notify("STATUS", JsonConvert.DeserializeObject<StatusMessage>(msg));
                    break;

                case "CONFIG_UPDATED":
                    obz.Notify("CONFIG_UPDATED", JsonConvert.DeserializeObject<ConfigMessage>(msg));
                    break;

                case "ERR":
                    obz.Notify("ERR", JsonConvert.DeserializeObject<ErrorMessage>(msg));
                    break;

                case "PLAYER_LEFT":
                    obz.Notify("PLAYER_LEFT", JsonConvert.DeserializeObject<PlayerLeftMessage>(msg));
                    break;

                case "PLAYER_JOIN":
                    obz.Notify("PLAYER_JOIN", JsonConvert.DeserializeObject<PlayerJoinMessage>(msg));
                    break;

                case "NEW_HOST":
                    obz.Notify("NEW_HOST", JsonConvert.DeserializeObject<NewHostMessage>(msg));
                    break;

                case "ROUND_PLAYING":
                    obz.Notify("ROUND_PLAYING", JsonConvert.DeserializeObject<RoundPlayingMessage>(msg));
                    break;

                case "HAND_UPDATED":
                    obz.Notify("HAND_UPDATED", JsonConvert.DeserializeObject<HandUpdatedMessage>(msg));
                    break;

                case "RESPONSE_PLAYED":
                    obz.Notify("RESPONSE_PLAYED", JsonConvert.DeserializeObject<BlankMessage>(msg));
                    break;

                case "GAME_KILLED":
                    obz.Notify("GAME_KILLED", JsonConvert.DeserializeObject<GameKilledMessage>(msg));
                    break;

                case "ROUND_PICKING":
                    obz.Notify("ROUND_PICKING", JsonConvert.DeserializeObject<RoundPickingMessage>(msg));
                    break;

                case "WINNER_PICKED":
                    obz.Notify("WINNER_PICKED", JsonConvert.DeserializeObject<WinnerPickedMessage>(msg));
                    break;

                case "GAME_COMPLETE":
                    obz.Notify("GAME_COMPLETE", JsonConvert.DeserializeObject<BlankMessage>(msg));
                    break;

                default:
                    Console.WriteLine("Unrecognised server status: {0}", semiParsed.type);
                    break;
            }
        }catch (Exception e){
            Console.WriteLine("Error parsing message: {0}", e.Message);
            Console.WriteLine(e.StackTrace);
        }
    }
}
