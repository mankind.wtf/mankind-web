using System.Collections.Generic;
using System;

public class Waiter {

    private readonly Observatory obz;

    private Dictionary<string, bool> _states = new Dictionary<string, bool>();

    public Waiter(Observatory obz){
        this.obz = obz;
    }

    public bool isBusy(){
        foreach(string key in _states.Keys){
            bool readSuccess = _states.TryGetValue(key, out bool val);
            if (readSuccess && val) return true;
        }
        return false;
    }

    public void SetState(string key, bool val){
        //Console.WriteLine("set state {0} to {1}", key, val);
        bool busyBefore = isBusy();

        bool existedBefore = _states.TryGetValue(key, out bool prevVal);
        _states[key] = val;

        if (!existedBefore || (prevVal != val))
            obz.Notify($"stateChanged_{key}", val);

        bool busyAfter = isBusy();
        if (busyBefore != busyAfter)
            obz.Notify("isBusyChanged", busyAfter);
    }

    // returns false by default. If you need it to be true by default, call SetState() before this method.
    public bool GetState(string key){
        return _states.GetValueOrDefault(key, false);
    }

    public bool StateSet(string key){
        return _states.ContainsKey(key);
    }
}