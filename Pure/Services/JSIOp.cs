using Microsoft.JSInterop;
using System.Threading.Tasks;
using System;

class CallbackHelper {
    private readonly Func<Task> callback;
    public CallbackHelper(Func<Task> cb){
        this.callback = cb;
    }
    
    [JSInvokable]
    public void Callback(){
        this.callback();
    }
}

public class JSIOp {
    private readonly IJSRuntime _jsRuntime;

    public JSIOp(IJSRuntime jsr){
        this._jsRuntime = jsr;
    }

    public async Task<int[]> ViewMeasures(){
        return await _jsRuntime.InvokeAsync<int[]>("blazorExtensions.ViewMeasures");
    }

    public async Task WriteCookie(string key, string val){
        await _jsRuntime.InvokeVoidAsync("blazorExtensions.WriteCookie", key, val, 1);
    }

    public async Task<string> ReadCookie(string key){
        return await _jsRuntime.InvokeAsync<string>("blazorExtensions.ReadCookie", key);
    }

    public async Task ClearCookie(string key){
        await _jsRuntime.InvokeVoidAsync("blazorExtensions.WriteCookie", key, "", -1);
    }

    // Asynchronously request fullscreen state to change. Takes different times on different devices.
    public void ToggleFullscreen(){
        _jsRuntime.InvokeVoidAsync("blazorExtensions.ToggleFullscreen");
    }

    public async Task InstallRotationHook(Func<Task> onChange){
        var oc = DotNetObjectReference.Create(new CallbackHelper(onChange));
        await _jsRuntime.InvokeVoidAsync("blazorExtensions.InstallRotationHook", oc);
    }

    public async Task InstallFullscreenHook(Func<Task> onChange){
        var oc = DotNetObjectReference.Create(new CallbackHelper(onChange));
        await _jsRuntime.InvokeVoidAsync("blazorExtensions.InstallFullscreenHook", oc);
    }

    public async Task<bool> IsFullscreen(){
        return await _jsRuntime.InvokeAsync<bool>("blazorExtensions.IsFullscreen");
    }
}