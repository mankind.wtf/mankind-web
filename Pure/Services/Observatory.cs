using System.Collections.Generic;
using System;

public class GenericObservable : IObservable<object> {
        List<IObserver<object>> _observers = new List<IObserver<object>>();

        public void Notify(object t)
        {
            foreach (var obs in _observers.ToArray()) obs.OnNext(t);
        }

        private class Unsubscriber : IDisposable
        {
            private List<IObserver<object>>_observers;
            private IObserver<object> _observer;

            public Unsubscriber(List<IObserver<object>> observers, IObserver<object> observer)
            {
                this._observers = observers;
                this._observer = observer;
            }

            public void Dispose()
            {
                if (_observer != null && _observers.Contains(_observer))
                    _observers.Remove(_observer);
            }
        }

        // Make sure observer implements Equals()
        public IDisposable Subscribe(IObserver<object> observer)
        {
            _observers.Add(observer);
            return new Unsubscriber(_observers, observer);
        }
}

class GenericObserver<T> : IObserver<object>{
    public virtual void OnCompleted(){}
    public virtual void OnError(Exception e){}
    public virtual void OnNext(object o){
        this.listener((T) o);
    }
    private Action<T> listener;
    private int id;

    public GenericObserver(Action<T> listener, int id){
        this.listener = listener;
        this.id = id;
    }

    // override object.Equals
    public override bool Equals(object obj){   
        if (obj == null || GetType() != obj.GetType())
            return false;
        
        return this.id == ((GenericObserver<T>) obj).id;
    }
    
    // override object.GetHashCode
    public override int GetHashCode()
    {
        return id;
    }
}

public class Observatory {

    private Dictionary<string, GenericObservable> _observables = new Dictionary<string, GenericObservable>();
    private Dictionary<string, List<IDisposable>> _disposers = new Dictionary<string, List<IDisposable>>();
    private int obsIdCounter {get;set;} = 0;

    public Observatory(){}

    public void Notify<T>(string where, T what){
        bool begotten = _observables.TryGetValue(where, out GenericObservable obs);
        if (begotten){
            obs.Notify(what);
        }
    }

    public void Register<T>(string when, Action<T> action, string id){ // todo synchronise
        bool begotten = _observables.TryGetValue(when, out GenericObservable obs);
        if (!begotten){
            var newObs = new GenericObservable();
            _observables.Add(when, newObs);
            obs = newObs;
        }

        var disp = obs.Subscribe(new GenericObserver<T>(action, obsIdCounter++));

        begotten = _disposers.TryGetValue(id, out List<IDisposable> componentDisposers);
        if (!begotten){
            componentDisposers = new List<IDisposable>();
            _disposers.Add(id, componentDisposers);
        }
        componentDisposers.Add(disp);
    }

    public void Deregister(string id){
        bool begotten = _disposers.TryGetValue(id, out List<IDisposable> componentDisposers);
        if (begotten)
            foreach(var disp in componentDisposers)
                disp.Dispose();
    }

}