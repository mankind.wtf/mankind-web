public class GameCreatedResponse {
    public long game_id {get; set;}
    public string player_auth{get; set;}
    public bool success {get; set;}
}