public class PlayerRanking {
    public string nick {get;set;}
    public int points {get;set;}
    public bool connected {get;set;}
    public bool host {get;set;}

    public PlayerRanking(){}

    public PlayerRanking(string nick){
        this.nick = nick;
        this.points = 0;
        this.connected = true;
        this.host = false;
    }
}

public class QuestionCard {
    public string text {get;set;}
    public int answers_required {get;set;}
}

public class ResponsePlay {
    public long id {get;set;}
    public string[] cards {get;set;}

    public string GetElID(){
        return $"response-play-{id}";
    }
}

public class ResponseDetail {
    public int count {get;set;}
    public ResponsePlay[] responses {get;set;} = null;

    public ResponseDetail(){
        this.count = 0;
        this.responses = new ResponsePlay[0];
    }
}

public class ResponseCard {
    public long card_id {get;set;}
    public string card_text {get;set;}

    public string GetElID(){
        return $"response-card-{card_id}";
    }
}