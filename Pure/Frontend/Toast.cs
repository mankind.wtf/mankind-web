using System.Collections.Generic;
using System;

public class ToastData : EventArgs{
    public string Content {get;set;}
    public bool Error {get;set;}

    public ToastData(string cont, bool err){
        this.Content = cont;
        this.Error = err;
    }

    public ToastData(string cont) : this(cont, false){
        
    }
}

public class Toast {
    public ToastData Data {get;set;}
    public int Id {get;set;}
    public bool Display {get;set;} = false;

    public Toast(ToastData t, int id){
        this.Data = t;
        this.Id = id;
    }

    public override bool Equals(object obj){
        if (!(obj is Toast)) return false;
        return ((Toast) obj).Id == this.Id;
    }
    public override int GetHashCode(){
        return Id;
    }

    public string elID(){
        return $"toast-{Id}";
    }

    public string GetOpacity(){
        return Display ? "1" : "0";
    }

    public string GetBGCol(){
        return Data.Error ? "red" : "lightgrey";
    }

    public string GetFGCol(){
        return Data.Error ? "white" : "black";
    }
}