using System.Collections.Generic;

public class BlankMessage : Message{

    public Dictionary<string, string> data {get;set;}
    public BlankMessage(string tipe) : base(tipe){
        data = new Dictionary<string, string>();
    }

}