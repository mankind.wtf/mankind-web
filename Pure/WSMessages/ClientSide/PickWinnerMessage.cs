public class PickWinnerData {
    public long response_id {get;set;}
    public PickWinnerData(long id){
        this.response_id = id;
    }
}

public class PickWinnerMessage : Message {

    public PickWinnerData data {get;set;}

    public PickWinnerMessage(long id) : base("PICK_WINNER"){
        this.data = new PickWinnerData(id);
    }

}