public class PlayResponseData {
    public long[] card_ids {get;set;}
    public PlayResponseData(long[] ids){
        this.card_ids = ids;
    }
}

public class PlayResponseMessage : Message {

    public PlayResponseData data {get;set;}
    public PlayResponseMessage(long[] card_ids) : base("PLAY_RESPONSE") {
        this.data = new PlayResponseData(card_ids);
    }

}