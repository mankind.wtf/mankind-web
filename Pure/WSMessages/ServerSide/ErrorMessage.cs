public class ErrorData {
    public string info {get;set;}
}

public class ErrorMessage : Message {

    public ErrorData data {get;set;}

    public ErrorMessage() : base("ERR") {

    }

}