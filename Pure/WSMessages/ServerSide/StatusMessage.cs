using System.Collections.Generic;

public class ResponseHistoryPoint {
    public string question {get;set;}
    public string[] responses {get;set;}
    public string card_czar {get;set;}
    public string winner {get;set;}
}

public class HumourPreference {
    public string card_czar {get;set;}
    public string responder {get;set;}
    public int perc_picked {get;set;}
}

public class StatusData{
        public string game_status {get;set;}
        public List<PlayerRanking> rankings {get;set;}

        public ResponseDetail response_detail {get;set;} = null;
        public RoundPlayingData in_game_data {get;set;} = null;
        
        public long winning_response_id {get;set;} = -1;
        public string winning_player {get;set;} = null;

        public ResponseHistoryPoint[] response_history {get;set;} = null;
        public HumourPreference[] stat_humour_pref {get;set;} = null;

}

public class StatusMessage : Message {

    public StatusData data {get;set;}

    public StatusMessage() : base("STATUS") {
        
    }

}