public class JustNickData {
    public string nick {get;set;}
}

public class PlayerLeftMessage : Message {

    public JustNickData data {get;set;}

    public PlayerLeftMessage() : base("PLAYER_LEFT") {

    }

}

public class PlayerJoinMessage : Message {

    public JustNickData data {get;set;}

    public PlayerJoinMessage() : base("PLAYER_JOIN") {

    }

}
public class NewHostMessage : Message {

    public JustNickData data {get;set;}

    public NewHostMessage() : base("NEW_HOST") {

    }

}