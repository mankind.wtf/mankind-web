using System.Collections.Generic;

public class RoundPlayingData {
    public string card_czar {get;set;}
    public QuestionCard question {get;set;}
    public int round_num {get;set;}
    public int sub_round_num {get;set;}
}

public class HandUpdatedData {
    public ResponseCard[] cards {get;set;}
}

public class RoundPickingData {
    public ResponseDetail response_detail {get;set;}
}

public class WinnerPickedData {
    public string winner_nick {get;set;}
    public long response_id {get;set;}
    public List<PlayerRanking> rankings {get;set;}
}

public class RoundPlayingMessage : Message {

    public RoundPlayingData data {get;set;}
    public RoundPlayingMessage() : base("ROUND_PLAYING"){

    }

}

public class HandUpdatedMessage : Message {

    public HandUpdatedData data {get;set;}
    public HandUpdatedMessage() : base("HAND_UPDATED"){

    }
}

public class RoundPickingMessage : Message {

    public RoundPickingData data {get;set;}

    public RoundPickingMessage() : base("ROUND_PICKING"){

    }

}

public class WinnerPickedMessage : Message {

    public WinnerPickedData data {get;set;}
    
    public WinnerPickedMessage() : base("WINNER_PICKED"){

    }

}