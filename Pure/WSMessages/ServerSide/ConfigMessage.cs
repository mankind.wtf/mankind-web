using System.Collections.Generic;

public class ConfigData {
    public Dictionary<string, string> config {get;set;}
}

public class ConfigMessage : Message {

    public ConfigData data {get;set;}

    public ConfigMessage() : base("CONFIG_UPDATED"){}

    public ConfigMessage(bool serverSide) : base(serverSide ? "CONFIG_UPDATED" : "SET_CONFIG") {

    }

}