public class GameKilledData {
    public string reason {get;set;}
}

public class GameKilledMessage : Message {

    public GameKilledData data {get;set;}

    public GameKilledMessage() : base("GAME_KILLED"){

    }

}