#!/bin/bash

# Kill old instances, if any. (There usually are)

old=`lsof -i:4997 | grep LISTEN | head -1 | cut -d' ' -f 3`

if [ ! -z $old ]
then
    kill -9 $old
fi

echo "Pre-launch done"
