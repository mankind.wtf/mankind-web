// own custom JS funcs for wasm interop
window.blazorExtensions = {
    WriteCookie: function (name, value, days) {

        var expires;
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        expires = "; expires=" + date.toGMTString();
        document.cookie = name + "=" + value + expires + "; path=/";
    },
    ReadCookie : function (key) {

        var name = key + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var carr = decodedCookie.split(';');
        for(var i = 0; i < carr.length; i++) {
            var c = carr[i].trim();
            if (c.indexOf(name) == 0) {
                return c.substring(name.length);
            }
        }
        return "";
    },
    ViewMeasures : function (){
        return [window.innerWidth, window.innerHeight];
    },
    ApplyStyle : function (elID, attrib, val){
        document.getElementById(elID).style[attrib] = val;
    },
    ToggleFullscreen : function(){
        if (window.blazorExtensions.IsFullscreen()){
            if (document.exitFullscreen) {
                document.exitFullscreen();
            } else if (document.mozCancelFullScreen) { /* Firefox */
                document.mozCancelFullScreen();
            } else if (document.webkitExitFullscreen) { /* Chrome, Safari and Opera */
                document.webkitExitFullscreen();
            } else if (document.msExitFullscreen) { /* IE/Edge */
                document.msExitFullscreen();
            }
        }else{
            var elem = document.documentElement;
            if (elem.requestFullscreen) {
                elem.requestFullscreen();
            } else if (elem.mozRequestFullScreen) { /* Firefox */
                elem.mozRequestFullScreen();
            } else if (elem.webkitRequestFullscreen) { /* Chrome, Safari and Opera */
                elem.webkitRequestFullscreen();
            } else if (elem.msRequestFullscreen) { /* IE/Edge */
                elem.msRequestFullscreen();
            }
        }
    },
    InstallRotationHook : function(dotnetHelper){
        window.addEventListener("orientationchange", function(){
            dotnetHelper.invokeMethodAsync("Callback");
        });
    },
    InstallFullscreenHook : function(dotnetHelper){
        /* Standard syntax */
        document.addEventListener("fullscreenchange", function() {
            dotnetHelper.invokeMethodAsync("Callback");
        });
        
        /* Firefox */
        document.addEventListener("mozfullscreenchange", function() {
            dotnetHelper.invokeMethodAsync("Callback");
        });
        
        /* Chrome, Safari and Opera */
        document.addEventListener("webkitfullscreenchange", function() {
            dotnetHelper.invokeMethodAsync("Callback");
        });
        
        /* IE / Edge */
        document.addEventListener("msfullscreenchange", function() {
            dotnetHelper.invokeMethodAsync("Callback");
        });
    },
    IsFullscreen : function(){
        return (window.fullScreen) ||
        (window.innerWidth == screen.width && window.innerHeight == screen.height);
    }
}
