using System.Threading.Tasks;
using Microsoft.AspNetCore.Components.WebAssembly.Hosting;
using Microsoft.Extensions.DependencyInjection;

namespace fsociety_web
{
    public class Program
    {
        public static async Task Main(string[] args)
        {
            var builder = WebAssemblyHostBuilder.CreateDefault(args);

            builder.Services.AddSingleton<Observatory>();
            builder.Services.AddSingleton<Waiter>();
            builder.Services.AddSingleton<JSIOp>();
            builder.Services.AddSingleton<Backend>();

            builder.RootComponents.Add<App>("app");

            builder.Services.AddBaseAddressHttpClient();

            var host = builder.Build();

            var back = host.Services.GetRequiredService<Backend>();
            await back.Initialise();

            await host.RunAsync();
        }
    }
}
